# Run-Mapper-React

This application is built as an accompaniment to [Run-Mapper-Cordova](https://gitlab.com/run-mapper/run-mapper). The proper way to install this is as a subrepo to the Cordova app. The instructions here assume that this is positioned as a sibling to the www directory within a Cordova app.

## How it works

This repo was built using [create-react-app](https://github.com/facebook/create-react-app). Many of your questions can be directed to their build process.

## How is this different

Cordova is not often associated with React for myriad reasons. In actuality, you can use whatever frontend tool you choose with Cordova. In this case, React is used and specifically, create-react-app is used to make this as easy as possible. One thing that was added to make this possible is an edit to the package.json file, in the 'scripts' section. Using the two lines below, the www directory that is a sibling to the run-mapper-react directory is first removed. Next, after the build, the build contents are moved to the www directory; then finally, edits to the index.html file remove some initial slashes in order for the links to be relative rather than absolute.

```
"prebuild": "rm -rf ../www/*",
"postbuild": "mv build/* ../www && sed -i \"\" 's/\\\"\\//\\\"/g' ../www/index.html",
```

## How to connect the pieces

Once the submodule has been added to run-mapper-cordova (see instructions at its repo), enter the run-mapper-react directory and run the following:

```
git submodule init
git submodule update
```
Once that has been done, run yarn (or npm install), and once that is complete:

```
yarn build
```

When that has been done, the app is ready to output from the parent repository, which is described at [Run-Mapper-Cordova](https://gitlab.com/run-mapper/run-mapper).
