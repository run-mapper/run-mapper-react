import React from 'react';
import ReactDOM from 'react-dom';
// import {Provider} from 'react-redux';
import {createStore} from 'redux';
// import createLogger from 'redux-logger';
import {AppContainer} from 'react-hot-loader';


import runmapData from './services/reducers';
import Root from './Root';

export const store = createStore(runmapData);

const rootElement = document.getElementById('root');

const unsubscribe = store.subscribe(function(){
  // type: confirm.type,
  // latLng: e.latLng,
  // marker
  console.log(store.getState());
  // this.runTrack
});

unsubscribe();

ReactDOM.render(
  <AppContainer>
    <Root store={store} />
  </AppContainer>,
  rootElement
);


/**
 *
 import React from 'react';
 // import ReactDOM from 'react-dom';
 import {Provider} from 'react-redux';
 import { BrowserRouter as Router, Route } from 'react-router-dom'
 import {createStore, applyMiddleware} from 'redux';

 import './index.css';
 import App from './App';
 import registerServiceWorker from './registerServiceWorker';


 ReactDOM.render(
   <App />, document.getElementById('root'));
 registerServiceWorker();

 *
 */
