import React, { Component } from 'react';
import Nav from './components/Nav';
import Main from './components/Main';
import Footer from './components/Footer';
import ErrorBoundary from './components/ErrorBoundary';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      currentScreen: 'Home',
      currentView: 'Home'
    };
  }
  changeState = currentScreen =>{
    console.log(currentScreen);
    this.setState({
      currentScreen:currentScreen,
      currentView:currentScreen,
    });
  }
  render() {
    return (
      <div className="wrapper">
        <Nav currentScreen={this.state.currentScreen} wrapper={this} />
        <ErrorBoundary>
          <Main currentView={this.state.currentView} wrapper={this} />
        </ErrorBoundary>
        <Footer currentView={this.state.currentView} wrapper={this} />
      </div>
    );
  }
}

export default App;
