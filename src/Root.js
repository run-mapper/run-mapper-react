import React from 'react';
import { Provider } from 'react-redux';
import { Switch, BrowserRouter as Router, Route } from 'react-router-dom';

import App from './App';
import Home from './components/Home';

const Root = ({ store }) => {
  return(
    <Provider store={store}>
      <Router>
        <Switch>
          <Route path="/" component={App} />
          <Route path="/Home" component={Home} />
        </Switch>
      </Router>
    </Provider>
  );
};

export default Root;
