import React, { Component } from 'react';
import MapContainer from './MapContainer';
// import store from '../../';


       // _store.dispatch({
      //   type: ADD_POINT,
      //   latLng: {
      //     lat:position.coords.latitude,
      //     lng:position.coords.longitude,
      //   },
      //   time: new Date().getTime()
      // });
      // firstTime.count = firstTime.count + 1;
      // alert(`noticeRun= ${position.coords.latitude}`);
      // if(firstTime.count<1){
      //   me.buildFirstMarker({
      //     lat:position.coords.latitude,
      //     lng:position.coords.longitude,
      //   });

class MapMaster extends Component {
  constructor(props){
    super(props);
    this.state={
      markers: []
    }
  }
  render() {
    const MapContainerCSS = {
      overflow: 'hidden',
      maxHeight: '75%',
      maxWidth: '50%',
      padding:0
    };
    return (
      <main style={MapContainerCSS}>
        <MapContainer markers={this.state.markers} />
      </main>
    );
  }
}

export default MapMaster;
