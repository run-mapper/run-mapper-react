import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
import React, { Component, Button } from 'react';
import ReactDOM from 'react-dom';
import { store } from '../';

import { ADD_RUN, ADD_STARTING_POINT, ADD_ENDING_POINT, ADD_POINT, SET_MARKERS } from '../services/actions';

const getOrientation = () => {
  return window.screen.availableWidth > window.screen.availableHeight;
}

export class MapContainer extends Component {
  state = {
    showingInfoWindow: false,
    initialState: false,
    orientation: getOrientation(),
    markers:[],
    startMarker: null,
    endMarker: null,
    currentMarker: 'startMarker',
    latLng: null,
    currentTime: 0
  };

  componentDidMount(){
    if(!store.getState().beginTime){
      const me = this;
      const firstTime = {count:-1};
      var onError =function(error) {
          alert('code: '    + error.code    + '\n' +
                'message: ' + error.message + '\n');
      };
      var onSuccess = function(position) {
        firstTime.count = firstTime.count + 1;
        if(firstTime.count<1){
          me.buildFirstMarker({
            lat:position.coords.latitude,
            lng:position.coords.longitude,
          });
        }
      };
      navigator.geolocation.getCurrentPosition(onSuccess, onError);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('componentDidUpdate');
    if (this.state.orientation !== getOrientation() ) {
      this.setState({ orientation: getOrientation() });
    }
    if(this.state.initialState===true){
      this.testMap(this.state, this.state.map, {
        msg: 'Do you want to use your current location as the starting point?',
        type: ADD_STARTING_POINT,
        marker: 'startMarker'
      }, true);
    }
  }

  removeAllMarkers = ()  => {
    if(this.state.markers){
      this.state.markers.forEach(marker => {
        marker.setMap(null);
      });
    }
    this.setState({
      markers: [],
      startMarker: null,
      endMarker: null
    })
  }

  removeMarker =  marker => {
    if(this.state[marker]){
      this.state[marker].setMap(null);
      // this.infoWindow.close();
    }
  }

  buildFirstMarker =  latLng => {
    const map = this.state.map;
    const firstMarker = new this.props.google.maps.Marker({
      position: {
        lat:latLng.lat,
        lng:latLng.lng
      },
      map: map,
      title: 'Start Position',
      name: 'startMarker'
    });
    const markers = [firstMarker]
      this.setState({
        latLng: latLng,
        [this.state.currentMarker]: firstMarker,
        markers,
        initialState: true
      });
      store.dispatch({
        type: SET_MARKERS,
        markers
      });

      // return this.marker;
      console.log('Built First Marker');
  }

  buildMarker =  latLng => {
    const map = this.state.map;
    let markers = this.state.markers;
    const nextMarker = new this.props.google.maps.Marker({
      position: {
        lat:latLng.lat,
        lng:latLng.lng
      },
      map: map,
      title: '-',
      name: '-'
    });
    markers.push(nextMarker);
    this.setState({
      markers
    });
    store.dispatch({
      type: ADD_POINT,
      latLng,
      markers,
      map
    });

      // this.infoWindow = new this.props.google.maps.InfoWindow ({
      //   content: this.mapQuestion.process(this),
      //   marker: this.state[this.state.currentMarker],
      //   visible: true
      // });
      // this.infoWindow.open(map, this.state[this.state.currentMarker]);
      // return this.marker;
      console.log('Built Marker');
      console.log(store.getState());
    // }
  };
  updateInfoAndMarker = (dest = null, map = this.state.map, visible = false, startFinish = null) => {
    console.log(visible);
    let updateState = {
      showingInfoWindow: visible,
      activeMarker: startFinish,
      map: map,
      dest: dest
    }
    if(startFinish==='startMarker'){
      updateState={
        showingInfoWindow: visible,
        startMarker: dest,
        activeMarker: 'startMarker',
        map: map,
        endMarker: this.state.endMarker,
      }
    }
    if(startFinish==='endMarker'){
      updateState={
        showingInfoWindow: visible,
        startMarker: this.state.startMarker,
        activeMarker: 'endMarker',
        map: map,
        endMarker: dest,
      }
    }
    this.setState({
      showingInfoWindow: visible,
      activeMarker: startFinish,
      map: map,
      dest: dest
    });
  }

  onMapLoad = (mapProps, map) => {
    this.setState({ map:map });
  }

  onMapClicked = (props, map, e) => {
    window.mapd=map;
    if (this.state.showingInfoWindow) {
      this.updateInfoAndMarker()
    }else{
      if(store.getState().beginTime === null){
        this.testMap(e, map, {
          msg: 'Do you want to use this point as starting point?',
          type: ADD_STARTING_POINT,
          marker: 'startMarker'
        });
      }else{
        this.testMap(e, map, {
          msg: 'Do you want to use this point as ending point?',
          type: ADD_ENDING_POINT,
          marker: 'endMarker'
        });
      }
    }
  };

  testMap = (e, map, confirm, first) => {
    if(window.confirm(confirm.msg)){
      store.dispatch({
        type: confirm.type,
        latLng: e.latLng,
        marker: confirm.marker,
        map
      });
      this.updateInfoAndMarker(e.latLng, map, true, confirm.marker);
    }else{
      this.removeMarker(confirm.marker);
      // this.updateInfoAndMarker(confirm.marker);
    }
    if(first){
      this.setState({initialState:false})
    }
  }

  getMapSize(){
    return this.state.orientation ? {
      paddingTop: '20vh',
      paddingHorizontal:'20vh',
      width: '90%',
      height: '50%',
      overflow: 'hidden'
    } : {
      paddingTop: '0vh',
      paddingHorizontal:'0vh',
      height: '80%',
      width: '100%',
      overflow: 'hidden'
    };
  }

  render() {
    const theCenter = this.state.latLng ? this.state.latLng : null;
    const mapsize = this.getMapSize();
    console.log(mapsize);
    return (
      <Map style={mapsize} google={this.props.google}
        onReady={this.onMapLoad}
        onClick={this.onMapClicked} center={theCenter}>
      </Map>
    )
  }
}

const YOUR_GOOGLE_API_KEY_GOES_HERE = 'YOUR_GOOGLE_API_KEY_GOES_HERE';

export default GoogleApiWrapper({
  apiKey: (YOUR_GOOGLE_API_KEY_GOES_HERE)
})(MapContainer);
