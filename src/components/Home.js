import React from 'react';

const Home = () => {
  return (
    <main>
      <h2>Run Mapper</h2>
      <p>
        Press Map to Start your Run. After confirming your starting point,
        you have five minutes to run!
      </p>
    </main>
  );
}

export default Home;
