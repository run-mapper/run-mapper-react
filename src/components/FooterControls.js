import React, { Component } from 'react';
import { store } from '../';
import { ADD_BEGIN, ADD_POINT, ADD_END } from '../services/actions';

// import Home from './Home';
// import MapMaster from './MapMaster';
// import Run from './Run';

// const FooterWrap = which => {
//   return   testMap = (e, map, confirm, first) => {
//       if(window.confirm(confirm.msg)){
//         store.dispatch({
//           type: confirm.type,
//           data: e.latLng,
//           map
//         });
//         this.updateInfoAndMarker(e.latLng, map, true, confirm.marker);
//       }else{
//         this.updateInfoAndMarker();
//       }
//       if(first){
//         this.setState({initialState:false})
//       }
//     }
// }

class FooterControls extends Component {
  constructor(props){
    super(props);
    this.state={
      timerInterval:null,
      isRunning: false,
      startTime: null,
      markers:[],
      startMarker: null,
      endMarker: null,
      currentMarker: 'startMarker',
      latLng: null
    };
  }
  componentDidMount(){
    const getCurrentMarker = () =>{
      return this.state[this.state.currentMarker];
    }
    function onError(error) {
        alert('code: '    + error.code    + '\n' +
              'message: ' + error.message + '\n');
    }
    if(!store.getState().beginTime){
      // alert('componentDidMount');
      const me = this;
      const firstTime = {count:-1};
      var onSuccess = function(position) {
        firstTime.count = firstTime.count + 1;
        if(firstTime.count<1){
          // alert(`footer.firstTime= ${firstTime.count}`);
        //   me.buildFirstMarker({
        //     lat:position.coords.latitude,
        //     lng:position.coords.longitude,
        //   });
        // }else{
        //   me.buildMarker({
        //     lat:position.coords.latitude,
        //     lng:position.coords.longitude,
        //   });
        }
      };
      navigator.geolocation.watchPosition(onSuccess, onError);
    }
  }
  makeIntoMinutes = num => {
    var mins=Math.floor(num/60), secs=Math.floor(num%60);
    return `0${mins}:${ secs>9 ? secs : '0' + secs }`;
  }
  cordovaInterface = argObj => {
    // queryLocation();
    switch(argObj.event){
      case 'enable':
      if(! window.enabled){
          window.newTimer=window.timer=new Date();
          window.cordova.plugins[argObj.plugin][argObj.event]();
          alert('You have five minutes!')
          window.enabled=true;
        }
        break;
      case 'disable':
        window.newTimer=new Date();
        alert(window.newTimer - window.timer);
        window.cordova.plugins[argObj.plugin][argObj.event]();
        alert('Cordova disable enabled.')
        break;
      case 'activate':
        break;
      case 'deactivate':
        break;
      case 'failure':
        break;
      default:
        break;
    }
  }
  enableBackground = () => {
    // window.
    this.cordovaInterface({
      plugin:'backgroundMode',
      event: 'enable'
    });
  }
  startRun = () => {
    // alert(JSON.stringify(store.getState().stored.markers));
    const me=this;
    this.enableBackground();
    store.dispatch({
      type: ADD_BEGIN,
      latLng: store.getState().stored.startingPoint.latLng,
      time: new Date().getTime(),
      timed: true,
      markers: store.getState().stored.markers.markers
    });
    this.oneMinuteTimer=setInterval(function(){
      me.noticeRun();
    },60000);
    this.fiveMinuteTimer=setTimeout(function(){
      me.endRun();
    },300000);
    this.timerInterval=setInterval(function(){
      var currentTimer=new Date();
      document.getElementById('timer').value=me.makeIntoMinutes(Math.round(
        (currentTimer.getTime()-store.getState().stored.beginTime.time)/1000
      ));
    },50)
    console.log(store.getState());
  };
  buildMarker =  latLng => {
    const map = this.state.map;
    let markers = this.state.markers;
    const nextMarker = new this.props.google.maps.Marker({
      position: {
        lat:latLng.lat,
        lng:latLng.lng
      },
      map: map,
      title: '-',
      name: '-'
    });
    markers.push(nextMarker);
    this.setState({
      markers
    });
    store.dispatch({
      type: ADD_POINT,
      latLng,
      markers,
      map
    });
  }
  noticeRun = () => {
    const me=this;
    const _store=store;
    var onError =function(error) {
        alert('code: '    + error.code    + '\n' +
              'message: ' + error.message + '\n');
    };
    var onSuccess = function(position) {
      const latLng = {
        lat:position.coords.latitude,
        lng:position.coords.longitude
      };
      _store.dispatch({
        type: ADD_POINT,
        latLng,
        time: new Date().getTime()
      });
      me.buildMarker(latLng);
      alert(`noticeRun= ${position.coords.latitude}`);
    };
    navigator.geolocation.getCurrentPosition(onSuccess, onError);

    console.log(store.getState())
    // var onSuccess = function(position) {
      // console.log(`noticeRun(position=${position.coords.latitude} and ${position.coords.longitude}`);
      // _store.dispatch({
      //   type: ADD_POINT,
      //   latLng: {
      //     lat:position.coords.latitude,
      //     lng:position.coords.longitude,
      //   },
      //   time: new Date().getTime()
      // });
      // me.buildMarker();
  };
  endRun = () => {
    const me=this;
    const _store=store;
    var rightNow = new Date().getTime();
    var onError =function(error) {
        alert('code: '    + error.code    + '\n' +
              'message: ' + error.message + '\n');
    };
    var onSuccess = function(position) {
      _store.dispatch({
        type: ADD_END,
        latLng: {
          lat:position.coords.latitude,
          lng:position.coords.longitude,
        },
        time: rightNow
      });
      alert(`endRun= ${position.coords.latitude}`);
    };
    navigator.geolocation.getCurrentPosition(onSuccess, onError);
    clearInterval(this.timerInterval);
    this.timerInterval=null;
      document.getElementById('timer').value=me.makeIntoMinutes(Math.round(
        (rightNow-store.getState().stored.beginTime.time)/1000
      ));
    console.log(store.getState());
  };
  render() {
    var startStyle ={
      color: '#fff',
      fontFamily: 'Roboto',
      fontSize: '14px',
      margin: '4px',
      cursor: 'pointer'
    }
    var endStyle ={
      color: '#fff',
      fontFamily: 'Roboto',
      fontSize: '14px',
      margin: '4px',
      cursor: 'pointer'
    }
    var inputStyle ={
      color: '#333',
      fontFamily: 'Roboto',
      fontSize: '14px',
      margin: '4px',
      cursor: 'pointer',
      textAlign: 'center'
    }
    switch(this.props.currentView){
      case 'Map':
      case 'Run':
      return (
        <div>
          <span style={startStyle} onClick={this.startRun}>Start Timed Run</span>
          <input style={inputStyle} type="text" id="timer"></input>
          <span style={endStyle} onClick={this.endRun}>End Timed Run</span>
          </div>
        );
      default:
        return <div>NO footer.</div>;
    }
  }
}

export default FooterControls;
