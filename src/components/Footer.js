import React, { Component } from 'react';
import FooterControls from './FooterControls';
const footer={
  background: 'black',
  color: 'white',
  textAlign: 'center',
  lineHeight: 2,
  height:'60px',
  zIndex: 210,
}

const foot = () => {
  return <div>FOOTER</div>;
};

class Footer extends Component {
  render() {
    let rendered;
    switch(this.props.currentView){
      case 'Home':
        rendered = foot();
        break;
      case 'Map':
        rendered = <FooterControls {...this.props} />;
        break;
      case 'Run':
        rendered = <FooterControls {...this.props} />;
        break;
      default:
        rendered = foot();
        break;
    }
    // let rendered = <p style={footer}>FOOTER</p>;
    return <footer style={footer}>{rendered}</footer>;
  }
}

export default Footer;

// import React, { Component } from 'react';
// import FooterControls from './FooterControls';
//
// const footer={
//   background: 'black',
//   color: 'white',
//   textAlign: 'center',
//   lineHeight: 2,
//   height:'60px',
//   zIndex: 210,
// }
//
// const foot = () => {
//   return `<p style=${footer}>FOOTER</p>`;
// };
//
// class Footer extends Component {
//   render() {
//     switch(this.props.currentView){
//       case 'Home':
//         return foot();
//       case 'Map':
//         return <FooterControls />;
//       case 'Run':
//         return <FooterControls />;
//       default:
//         return foot();
//     }
//   }
// }
//
// export default Footer;
