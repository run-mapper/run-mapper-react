import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Nav.css'
import { store } from '../';
import { SET_VIEW } from '../services/actions';

class NavButton extends Component {
  selectScreen = () => {
    this.props.wrapper.selectScreen(this.props.NavDest);
    store.dispatch({
      type: SET_VIEW,
      data: this.props.NavDest
    });
  }
  render(){
    return (
      <li className={this.props.currentScreen === this.props.NavDest ? 'selected' : 'defaulted'} onClick={this.selectScreen}>
        <Link to={`/${this.props.NavDest}`}>{this.props.NavDest}</Link>
      </li>
    );
  }
}

class Nav extends Component {
  getCurrentScreen = () => {
    return this.props.currentScreen;
  }
  selectScreen = which => {
    this.props.wrapper.changeState(which);
  }
  render() {
    return (
      <nav>
        <NavButton currentScreen={this.props.currentScreen} wrapper={this} NavDest="Home" />
        <NavButton currentScreen={this.props.currentScreen} wrapper={this} NavDest="Map" />
      </nav>
    );
  }
}

export default Nav;
