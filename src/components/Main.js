import React, { Component } from 'react';

import Home from './Home';
import MapMaster from './MapMaster';

class Main extends Component {
  render() {
    switch(this.props.currentView){
      case 'Home':
        return <Home wrapper={this.props.wrapper} />;
      case 'Map':
        return <MapMaster />;
      default:
        return <Home />;
    }
  }
}

export default Main;
