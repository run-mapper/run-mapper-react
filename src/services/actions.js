import {v4 as generateId} from 'node-uuid';

export const ADD_STARTING_POINT = 'ADD_STARTING_POINT';
export const ADD_POINT = 'ADD_POINT';
export const ADD_ENDING_POINT = 'ADD_ENDING_POINT';
export const ADD_BEGIN = 'ADD_BEGIN';
export const ADD_END = 'ADD_END';
export const SET_VIEW = 'SET_VIEW';
export const SET_MARKERS = 'SET_MARKERS';

export const getLocation = id => ({
  type: ADD_END,
  id
});

export const setMarkers = data => ({
  type: SET_MARKERS,
  data
});

export const setView = view => ({
  type: SET_VIEW,
  data: {data: view}
});

export const addBegin = data => ({
  type: ADD_BEGIN,
  data
});

export const addEnd = data => ({
  type: ADD_END,
  data
});

export const addStartingPoint = data => ({
  type: ADD_STARTING_POINT,
  id: generateId(),
  data
});

export const addPoint = data => ({
  type: ADD_POINT,
  id: generateId(),
  data
});

export const addEndingPoint = data => ({
  type: ADD_ENDING_POINT,
  id: generateId(),
  data
});
//
//
// export const ADD_LOCATION = 'ADD_LOCATION';
// export const GET_LOCATION = 'GET_LOCATION';
// export const GET_LOCATIONS = 'GET_LOCATIONS';
//
// export const ADD_RUN = 'ADD_RUN';
// export const GET_RUN = 'GET_RUN';
// export const GET_RUNS = 'GET_RUNS';
// export const GET_VIEW = 'GET_VIEW';
//
// export const getLocations = () => ({
//   type: GET_LOCATIONS
// });
//
// export const getView = data => ({
//   type: GET_VIEW,
//   data
// });
//
// export const addRun = (name, data) => ({
//   type: ADD_RUN,
//   id: generateId(),
//   name,
//   data
// });
//
// export const getRun = id => ({
//   type: GET_RUN,
//   id
// });
//
// export const getRuns = () => ({
//   type: GET_RUNS
// });
//
