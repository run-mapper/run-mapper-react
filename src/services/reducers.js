import {combineReducers} from 'redux';
import { store } from '../';
import {
  SET_VIEW,
  SET_MARKERS,
  ADD_STARTING_POINT,
  ADD_POINT,
  ADD_ENDING_POINT,
  ADD_BEGIN,
  ADD_END
} from './actions';

const initialState = {
  currentView:{
    data: 'Map'
  }
};

const stored = (state = initialState, action) => {
  switch (action.type) {
    case SET_VIEW:
      return {
        ...state,
        currentView: action
      };
    case SET_MARKERS:
      return {
        ...state,
        markers: action
      };
    case ADD_BEGIN:
      return {
        ...state,
        beginTime: action
      };
    case ADD_END:
      return {
        ...state,
        endTime: action
      };
    case ADD_POINT:
      return {
        ...state,
        point: action
      };
    case ADD_STARTING_POINT:
      return {
        ...state,
        startingPoint: action
      };
    case ADD_ENDING_POINT:
      return {
        ...state,
        endingPoint: action
      };
    default:
      return state;
  }
};

const runmapData = combineReducers({
  stored
});

export default runmapData;
